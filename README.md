# dotgen

Dotfiles generator

## Configuring dotgen
In order for dotgen to work, a config must be specified at `~/.dotgen/config.js`. For an example config, see https://gitlab.com/jedfong/dotgen/tree/master/sample-configuration.

## Generate dotfiles
Once the configuration exists at `~/.dotgen/config.js`, dotgen can be run.
```
npx dotgen
```
