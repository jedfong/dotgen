#!/usr/bin/env node
const os = require('os');
const path = require('path');

const DOTCONFIG_PATH = path.join(os.homedir(), '.dotgen');
const config = require(path.join(DOTCONFIG_PATH, 'config.js'));

require('./src/bashrc')({ DOTCONFIG_PATH, config: (config && config.bashrc) || {} });
require('./src/symlinks')({ DOTCONFIG_PATH, config: (config && config.symlinks) || {} });
