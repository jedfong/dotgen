module.exports = {
  bashrc: {
    path: 'bashrc', // relative directory for finding all files (any non-hidden files will be written to the ~/.bashrc
  },
  symlinks: {
    path: 'symlinks', // relative directory for finding all files
    files: [
      '.gitconfig', // if a string, then the same as defining in/out with this value
      {
        in: 'global-gitignore',
        out: '.gitignore_global',
        prelink: () => console.log('before symlinking'),
        postlink: () => console.log('after symlinking'),
      },
    ],
  },
};
