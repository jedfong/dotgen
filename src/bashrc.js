const fs = require('fs');
const path = require('path');
const os = require('os');

const BASH_PROFILE_PATH = path.join(os.homedir(), '.bash_profile');
const BASHRC_PATH = path.join(os.homedir(), '.bashrc');

const GEN_START = '# GENERATED_BASHRC_START';
const GEN_END = '# GENERATED_BASHRC_END';

const installBashrc = ({ DOTCONFIG_PATH, config }) => {
  if (!fs.existsSync(BASH_PROFILE_PATH)) {
    const lines = [
      '#!/bin/bash',
      '[[ -s ~/.bashrc ]] && source ~/.bashrc',
      '',
    ];
    fs.writeFileSync(BASH_PROFILE_PATH, lines.join(os.EOL), { mode: 0o666 });
  }

  if (!fs.existsSync(BASHRC_PATH)) {
    fs.writeFileSync(BASHRC_PATH, '', { mode: 0o666 });
  }

  generateBashrc(path.join(DOTCONFIG_PATH, config.path || 'bashrc'));
};


const generateBashrc = srcPath => {
  const existingLines = fs.readFileSync(BASHRC_PATH, 'utf8').split(os.EOL);
  const genFirstIdx = existingLines.findIndex(item => item === GEN_START);
  const genLastIdx = existingLines.findIndex(item => item === GEN_END);

  let beforeLines;
  let afterLines;
  if (genFirstIdx !== -1 && genLastIdx !== -1) {
    beforeLines = existingLines.slice(0, genFirstIdx);
    afterLines = existingLines.slice(genLastIdx + 1);
  } else {
    beforeLines = existingLines;
    afterLines = [];
  }

  const newLines = [
    ...beforeLines,
    GEN_START,
    '# Generated via https://gitlab.com/jedfong/dotgen/',
    `# ${new Date()}`,
    generateBashrcContent(srcPath),
    GEN_END,
    ...afterLines,
  ];

  fs.writeFileSync(BASHRC_PATH, newLines.join(os.EOL), { mode: 0o666 });
};

const generateBashrcContent = srcPath => {
  return fs.readdirSync(srcPath).reduce((acc, fileName) => {
    if (fileName.startsWith('.')) {
      return acc;
    }
    return [
      ...acc,
      fs.readFileSync(path.join(srcPath, fileName), 'utf8'),
    ];
  }, []).join(os.EOL);
};

module.exports = installBashrc;
