const fs = require('fs');
const os = require('os');
const path = require('path');

const installSymlinks = ({ DOTCONFIG_PATH, config }) => {
  const filesPath = path.join(DOTCONFIG_PATH, config.path || 'symlinks');
  (config.files || []).forEach(file => {
    const inPath = getInPath(file, filesPath);
    const outPath = getOutPath(file);

    if (inPath === null || outPath === null) {
      return;
    }
    symlink(inPath, outPath, getPrelink(file), getPostlink(file));
  });
};

const getInPath = (file, filesPath) => {
  switch (typeof file) {
    case 'string':
      return path.resolve(filesPath, file);
    case 'object':
      return file.in ? path.resolve(filesPath, file.in) : null;
    default:
      return null;
  }
};

const getOutPath = file => {
  switch (typeof file) {
    case 'string':
      return path.resolve(os.homedir(), file);
    case 'object':
      return file.out ? path.resolve(os.homedir(), file.out) : null;
    default:
      return null;
  }
};

const getPrelink = file => ((typeof file === 'object' && typeof file.prelink === 'function') ? file.prelink : () => {});
const getPostlink = file => ((typeof file === 'object' && typeof file.postlink === 'function') ? file.postlink : () => {});

const symlink = (inPath, outPath, prelink, postlink) => {
  if (fs.existsSync(outPath)) {
    fs.unlinkSync(outPath);
  }
  if (!fs.existsSync(path.dirname(outPath))) {
    // ensure that path is created first
    fs.mkdirSync(path.dirname(outPath), { recursive: true });
  }
  prelink();
  const lstat = fs.lstatSync(inPath);
  if (lstat.isFile()) {
    fs.symlinkSync(inPath, outPath, 'file');
  } else if (lstat.isDirectory()) {
    fs.symlinkSync(inPath, outPath, 'dir');
  }
  postlink();
};

module.exports = installSymlinks;
